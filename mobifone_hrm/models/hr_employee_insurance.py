from odoo import _, api, fields, models
from odoo.exceptions import UserError, ValidationError

class HrEmployeeInsurance(models.Model):
    _name = "hr.employee"
    _inherit = "hr.employee"
    
    insurance = fields.Char(string="Bảo hiểm")
    card_number_insurance = fields.Char(string="Số thẻ bảo hiểm y tế")
    provincial_code = fields.Char(string="Mã tỉnh cấp") 
    place_register_medical = fields.Char(string="Nơi đăng ký khám chữa bệnh") 
    insurance_info_lines = fields.One2many('insurance.infomation.lines', 'insurance_info_id', string="Thông tin bảo hiểm")
    
