import re
from datetime import date

from odoo import _, api, fields, models
from odoo.exceptions import ValidationError


class HrEmployeeInherit(models.Model):
    _name="hr.employee"
    _inherit="hr.employee"

    employee_idd = fields.Char(string = "Mã nhân viên")
    date_of_birth_id = fields.Date(string = "Ngày sinh")
    gender_id = fields.Selection([('male', "Nam"), ('female', 'Nữ')], string = "Giới tính")
    place_of_birth = fields.Char(string = "Nơi sinh")
    address = fields.Char(string = "Quê quán")
    identification = fields.Char(string = "CMT/CCCD/Hộ chiếu")
    data_of_start_identification = fields.Date(string = "Ngày cấp")
    place_of_identification = fields.Char(string = "Nơi cấp")
    country = fields.Many2one('res.country', string = "Quốc tịch")
    marital_id = fields.Selection([('single', 'Độc thân'),('married','Đã kết hôn'), ('divorced', 'Đã ly hôn')], string="Tình trạng hôn nhân" )
    ethnic = fields.Selection(selection="list_ethnics", string = "Dân tộc")
    religion = fields.Selection(selection="list_religions" , string = "Tôn giáo")
    account_number = fields.Char(string = "Số tài khoản")
    account_name = fields.Char(string="Tên tài khoản")
    bank = fields.Char(string = "Ngân hàng")
    bank_branch = fields.Char(string = "Chi nhánh ngân hàng")
    personal_tax_code = fields.Char(string = "Mã số thuế cá nhân")
    date_of_entry_to_work = fields.Date(string = "Ngày vào làm")
    official_start_date = fields.Date(string = "Ngày vào làm chính thức")
    level_common = fields.Selection(selection="list_educationLevel", string = "Trình độ phổ thông")
    certificate_id = fields.Selection(selection="list_highestProfessional", string = "Chuyên môn cao nhất")
    study_field = fields.Char(string = "Lĩnh vực nghiên cứu")
    study_school = fields.Char(string ="Trường học")
    phone_id = fields.Char(string = "Số điện thoại")
    email_id = fields.Char(string = "Email")
    accommodation_current = fields.Char(string = "Chỗ ở hiện tại")
    permanent_address = fields.Char(string = "Địa chỉ thường trú")
    information_family_lines = fields.One2many('information.family.lines', 'information_employee')
    work_experience = fields.One2many('work.experience','workExperience_employee')
    contract_ids = fields.One2many('hr.contract', 'employee_id')
    contract_job = fields.One2many('hr.contract', 'employee_id')

    job_id_contract = fields.Many2one(related='contract_job.job_id', string="Vị trí làm việc")
    name_contract_id = fields.Char(related='contract_job.name', string ="Mã hợp đồng lao động")
    contract_type_contract_id = fields.Many2one(related='contract_job.contract_type_id', string='Kiểu hợp đồng lao động')
    wage_contract = fields.Monetary(related="contract_job.wage", currency_field='currency_id')
    currency_id = fields.Many2one('res.currency', string='Currency', default=lambda self: self.env.company.currency_id)
    department_contract_id = fields.Many2one(related='contract_job.department_id', string="Phòng/Ban")
    branch_contract = fields.Char(related="contract_job.branch_contract", string="Chi nhánh")
    work_location_contract = fields.Char(related="contract_job.work_location_contract",string ="Địa điểm làm việc")
    child_ids = fields.One2many('hr.employee', 'parent_id', string='Direct subordinates')
    department_id = fields.Many2one(
        'hr.department', 'Phòng/Ban', domain="['|', ('company_id', '=', False), ('company_id', '=', company_id)]")
    work_phone = fields.Char(
        'Điện thoại công ty', default="0", store=True, readonly=False)
    mobile_phone = fields.Char('Số di động làm việc', default="0")
    work_email = fields.Char('Email công việc')
    coach_id = fields.Many2one(
        'hr.employee', 'Người huấn luyện', compute='_compute_coach', store=True, readonly=False,
        domain="['|', ('company_id', '=', False), ('company_id', '=', company_id)]",
        help='Select the "Employee" who is the coach of this employee.\n'
        'The "Coach" has no specific rights or responsibilities by default.')
    parent_id = fields.Many2one('hr.employee', 'Người quản lý', compute="_compute_parent_id", store=True, readonly=False,
                                domain="['|', ('company_id', '=', False), ('company_id', '=', company_id)]")

    @api.constrains('date_of_birth_id')
    def _check_dates_birth(self):
        today = date.today()
        if any(self.filtered(lambda rec:rec.date_of_birth_id and rec.date_of_birth_id >= today)):
            raise ValidationError(("Ngày tháng năm sinh không được lớn hơn hoặc bằng ngày hiện tại"))

    @api.constrains('personal_tax_code')
    def _check_personal_tax_code(self):
        if self.personal_tax_code:
            match = re.match('^[0-9]', self.personal_tax_code)
            if not match:
                raise ValidationError('Mã số thuế cá nhân vui lòng chỉ nhập số')

    @api.constrains('account_number')
    def _check_account_number(self):
        if self.account_number:
            match = re.match('^[0-9]', self.account_number)
            if not match:   
                raise ValidationError('Số tài khoản vui lòng chỉ nhập số')

    @api.constrains('identification')
    def _check_identification(self):
        if self.identification:
            match = re.match('^[0-9]', self.identification)
            if not match:   
                raise ValidationError('CMT/CCCD/Hộ chiếu vui lòng chỉ nhập số')

    @api.constrains('phone_id')
    def _check_phone_id(self):
        for rec in self:
            if rec.phone_id and len(rec.phone_id) != 10:
                raise ValidationError(_("Số điện thoại bạn nhập sai, vui lòng nhập đủ 10 số !!!"))
            if self.phone_id:
                match = re.match('^[0-9]\d{9}$', self.phone_id)
                if not match:
                    raise ValidationError('Vui chỉ nhập số')
        return True

    @api.constrains('email_id')
    def _check_email_format(self):
        for record in self:
            if record.email_id and not re.match(r"[^@]+@[^@]+\.[^@]+", record.email_id):
                raise ValidationError("Vui lòng nhập dúng định dạng")

    @api.constrains('date_of_entry_to_work', 'official_start_date')
    def _check_dates(self):
        today = date.today()
        if any(self.filtered(lambda rec: rec.date_of_entry_to_work and rec.official_start_date and rec.date_of_entry_to_work >= rec.official_start_date)):
            raise ValidationError(_("Ngày vào làm phải trước Ngày chính thức"))
        if any(self.filtered(lambda rec: rec.date_of_entry_to_work and rec.date_of_entry_to_work > today)):
            raise ValidationError(
                _("Ngày vào lằm không được lớn hơn ngày hiện tại"))
        elif any(self.filtered(lambda rec: rec.official_start_date and rec.official_start_date > today)):
            raise ValidationError(
                _("Ngày làm chính thức không được lớn hơn ngày hiện tại"))
        return True

    def list_ethnics(self):
        return [('Kinh','Kinh'),('Tày','Tày'),('Thái','Thái'),('Hoa','Hoa'),('Khơ-me','Khơ-me'),('Mường','Mường'),
                ('Nùng','Nùng'),('Hmông','Hmông'),('Dao','Dao'),('Gia-rai','Gia-rai'),('Ngái','Ngái'),('Ê-đê','Ê-đê'),
                ('Ba-na','Ba-na'),('Xơ-đăng','Xơ-đăng'),('Sán Chay','Sán Chay'),('Cơ-ho','Cơ-ho'),('Chăm','Chăm'),
                ('Sán Dìu','Sán Dìu'),('Hrê','Hrê'),('Mnông','Mnông'),('Ra-glai','Ra-glai'),('Xtiêng','Xtiêng'),
                ('Bru-Vân Kiều','Bru-Vân Kiều'),('Thổ','Thổ'),('Giáy','Giáy'),('Cơ-tu','Cơ-tu'),('Gié-Triêng','Gié-Triêng'),
                ('Mạ','Mạ'),('Khơ-mú','Khơ-mú'),('Co','Co'),('Ta-ôi','Ta-ôi'),('Chơ-ro','Chơ-ro'),('Kháng','Kháng'),
                ('Xinh-mun','Xinh-mun'),('Hà Nhì','Hà Nhì'),('Chu-ru','Chu-ru'),('Lào','Lào'),
                ('La Chi','La Chi'),('La Ha','La Ha'),('Phù Lá','Phù Lá'),('La Hủ','La Hủ'),
                ('Lự','Lự'),('Lô Lô','Lô Lô'),('Chứt','Chứt'),('Mảng','Mảng'),('Pà Thẻn','Pà Thẻn'),
                ('Cơ Lao','Cơ Lao'),('Cống','Cống'),('Bố Y','Bố Y'),('Si La','Si La'),('Pu Péo','Pu Péo'),
                ('Brâu','Brâu'),('Ơ Đu','Ơ Đu'),('Rơ-măm','Rơ-măm'),('Ca dong','Ca dong')]
    
    def list_religions(self):
        return [('Không','Không'),('Hiếu Nghĩa','Hiếu Nghĩa'),('Phật Giáo','Phật Giáo'),
                ('Thống nhất','Thống nhất'),('Công Giáo','Công Giáo'),('Hòa Hảo','Hòa Hảo'),
                ('Cao Đài','Cao Đài'),('Tin Lành','Tin Lành'),('Hồi Giáo','Hồi Giáo'),
                ('Bà La Môn','Bà La Môn'),('Thiền Lâm','Thiền Lâm'),('bskh','bskh'),
                ('Từ Ân','Từ Ân'),('Bà Ni','Bà Ni'),('Sán Dìu','Sán Dìu')]

    def list_educationLevel(self):
        return [('1/12','1/12'),('2/12','2/12'),('3/12','3/12'),('4/12','4/12'),('5/12','5/12'),
        ('6/12','6/12'),('7/12','7/12'),('8/12','8/12'),('9/12','9/12'),
        ('10/12','10/12'),('11/12','11/12'),('12/12','12/12'),]

    def list_highestProfessional(self):
        return [('Sơ cấp','Sơ cấp'),('Trung cấp','Trung cấp'),('Cao đẳng','Cao đẳng'),
        ('Kỹ sư','Kỹ sư'),('Cử nhân','Cử nhân'),('Thạc sĩ','Thạc sĩ'),('Tiến sĩ','Tiến sĩ'),
        ('Tiến sĩ khoa học','Tiến sĩ khoa học'),]

