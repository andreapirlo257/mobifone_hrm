from datetime import date
from odoo import _, api, fields, models
from odoo.exceptions import ValidationError


class InsurancePaymentInformation(models.Model):
    _name = "insurance.infomation.lines"
    _description = "Insurance Payment Information"

    insurance_info_id = fields.Many2one("hr.employee", string="Thông tin đóng bảo hiểm")
    so_from_month = fields.Date(string="Từ tháng"  )
    so_formality = fields.Char(string="Hình thức" )
    so_reason = fields.Char(string="Lý do")
    so_pay_company = fields.Float(string="CT đóng", widget="percentage")
    so_pay_workers = fields.Float(string="NLĐ đóng", widget="percentage")
    ac_pay_company = fields.Float(string="CT đóng", widget="percentage" )
    ac_pay_workers = fields.Float(string="NLĐ đóng", widget="percentage" )
    he_pay_company = fields.Float(string="CT đóng", widget="percentage")
    he_pay_workers = fields.Float(string="NLĐ đóng", widget="percentage")
    un_pay_company = fields.Float(string="CT đóng", widget="percentage")
    un_pay_workers = fields.Float(string="NLĐ đóng")
    insurance_premium_rate = fields.Integer(string="Mức đóng BH")
    un_pay_company_main = fields.Integer(string="Công ty đóng")
    un_pay_workers_main = fields.Integer(string="NLĐ đóng")

    @api.constrains('so_from_month', 'so_formality', 'so_reason', 'so_pay_company', 'so_pay_workers', 'ac_pay_company', 'ac_pay_workers', 'he_pay_company', 'he_pay_workers', 'un_pay_company', 'un_pay_workers')
    def _check_payment_values(self):
        for record in self: 
            if not (0 <= record.so_pay_company <= 1): 
                raise ValidationError(_("Vui lòng nhập lại CT đóng của Bảo hiểm xã hội\nGiá trị nhập vào chỉ từ 0 đến 100%"))
            if not (0 <= record.so_pay_workers <= 1): 
                raise ValidationError(_("Vui lòng nhập lại NLĐ đóng của Bảo hiểm xã hội\nGiá trị nhập vào chỉ từ 0 đến 100%"))
            if not (0 <= record.ac_pay_company <= 1): 
                raise ValidationError(_("Vui lòng nhập lại CT đóng của Bảo hiểm TNLĐ\nGiá trị nhập vào chỉ từ 0 đến 100%"))
            if not (0 <= record.ac_pay_workers <= 1): 
                raise ValidationError(_("Vui lòng nhập lại NLĐ đóng của Bảo hiểm TNLĐ\nGiá trị nhập vào chỉ từ 0 đến 100%"))
            if not (0 <= record.he_pay_company <= 1): 
                raise ValidationError(_("Vui lòng nhập lại CT đóng của Bảo hiểm y tế\nGiá trị nhập vào chỉ từ 0 đến 100%"))
            if not (0 <= record.he_pay_workers <= 1): 
                raise ValidationError(_("Vui lòng nhập lại NLĐ đóng của Bảo hiểm y tế\nGiá trị nhập vào chỉ từ 0 đến 100%"))
            if not (0 <= record.un_pay_company <= 1): 
                raise ValidationError(_("Vui lòng nhập lại CT đóng của Bảo hiểm thất nghiệp\nGiá trị nhập vào chỉ từ 0 đến 100%"))
            if not (0 <= record.un_pay_workers <= 1): 
                raise ValidationError(_("Vui lòng nhập lại NLĐ đóng của Bảo hiểm thất nghiệp\nGiá trị nhập vào chỉ từ 0 đến 100%"))