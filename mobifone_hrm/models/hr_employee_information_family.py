import re
from datetime import date

from openerp.exceptions import UserError

from odoo import _, api, fields, models
from odoo.exceptions import ValidationError


class InformationFamily(models.Model):
    _name="information.family.lines"
    _description="Information Family Lines"

    information_employee = fields.Many2one('hr.employee', string="information Employee")
    relationship = fields.Char(string="Mối quan hệ", required=True)
    name_rel = fields.Char(string="Họ và tên", required=True)
    year_of_birth_ref = fields.Date(string ="Năm sinh", required=True)
    job_rel = fields.Char(string="Nghề nghiệp", required=True)
    phone_rel = fields.Char(string="Số điện thoại", required=True)
    dependent_person_rel = fields.Char(string="Người phụ thuộc", required=True)

    @api.constrains('phone_rel')
    def _check_phone_rel(self):
        for rec in self:
            if rec.phone_rel and len(rec.phone_rel) != 10:
                raise ValidationError(_("Số điện thoại bạn nhập sai, vui lòng nhập đủ 10 số !!!"))
            if self.phone_rel:
                match = re.match('^[0-9]\d{9}$', self.phone_rel)
                if not match:
                    raise ValidationError(_('Vui chỉ nhập số'))
        return True


    @api.constrains('year_of_birth_ref')
    def _check_dates_birth(self):
        today = date.today()
        if any(self.filtered(lambda rec: rec.year_of_birth_ref and rec.year_of_birth_ref >= today)):
            raise ValidationError(_("Ngày tháng năm sinh không được lớn hơn hoặc bằng ngày hiện tại'"))          
