import re
from datetime import date
from odoo import _, api, fields, models
from odoo.exceptions import ValidationError


class WorkExperience(models.Model):
    _name="work.experience"
    _description="Work Experience"  

    workExperience_employee = fields.Many2one('hr.employee', string=" work Experience")
    from_day_work = fields.Date(string= "Từ ngày", required=True)
    end_day_work = fields.Date(string = "Đến ngày", required=True)
    position_work = fields.Char(string = "Vị trí làm việc", required=True)
    reference_work = fields.Char(string = "Người tham chiếu", required=True)
    description_work = fields.Char(string="Mô tả công việc", required=True)
    company_work = fields.Char(string ="Công ty", required=True)

    @api.constrains('from_day_work', 'end_day_work')
    def _check_dates(self):
        today = date.today()
        if any(self.filtered(lambda rec: rec.from_day_work and rec.end_day_work and rec.from_day_work >= rec.end_day_work)):
            raise ValidationError(_("Ngày bắt đầu phải trước Ngày kết thúc"))
        if any(self.filtered(lambda rec: rec.from_day_work and rec.from_day_work >= today)):
            raise ValidationError(
                _("Ngày bắt đầu không được lớn hơn ngày hiện tại"))
        elif any(self.filtered(lambda rec: rec.end_day_work and rec.end_day_work > today)):
            raise ValidationError(
                _("Ngày kết thúc không được lớn hơn ngày hiện tại"))
        return True